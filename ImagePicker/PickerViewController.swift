//
//  PickerViewController.swift
//  ImagePicker
//
//  Created by Nhân Phùng on 1/25/18.
//  Copyright © 2018 YangCS. All rights reserved.
//

import UIKit
import Photos

private let reuseIdentifier = "Cell"

protocol PickerDelegate {
    func getImage(images: [UIImage])
}

class PickerViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{

    var imageAssetsResult: PHFetchResult<PHAsset>!
    var listAssets: [PHAsset] = []
    var ImageSelects: [UIImage] = []
    var delegate: PickerDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        self.collectionView!.register(PickerCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        // Do any additional setup after loading the view.
        
        listAssets = getImageAssets()
        self.collectionView?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listAssets.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PickerCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PickerCollectionViewCell
        
        cell.setItem(asset: listAssets[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let currentCell = collectionView.cellForItem(at: indexPath) as! PickerCollectionViewCell
        if currentCell.layer.borderWidth == 2 {
            currentCell.layer.borderWidth = 0
            currentCell.layer.borderColor = UIColor.clear.cgColor
            ImageSelects.remove(at: ImageSelects.index(of: currentCell.imageView.image!)!)
        }else{
            currentCell.layer.borderWidth = 2
            currentCell.layer.borderColor = UIColor.red.cgColor
            ImageSelects.append(currentCell.imageView.image!)
        }
        currentCell.clipsToBounds = true
    }

    
    //PHAsset
    
    func getImageAssets() -> [PHAsset] {
        imageAssetsResult = PHAsset.fetchAssets(with: .image, options: nil)
        var assets: [PHAsset] = []
        for item in 0...imageAssetsResult.count - 1 {
            assets.append(imageAssetsResult[item])
        }
        
        return assets
    }
    @IBAction func didClickOk(_ sender: Any) {
        self.delegate?.getImage(images: self.ImageSelects)
        dismiss(animated: true, completion: nil)
    }
  
    @IBAction func didClickCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
