//
//  ViewController.swift
//  ImagePicker
//
//  Created by Nhân Phùng on 1/24/18.
//  Copyright © 2018 YangCS. All rights reserved.
//

import UIKit
import Photos

@objc protocol aaaa {
   @objc optional func print()
    
}

class ViewController: UIViewController, UINavigationControllerDelegate,UIImagePickerControllerDelegate, PickerDelegate,aaaa {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    
    let imagePicker = UIImagePickerController();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        imagePicker.delegate = self
    }
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getImage(images: [UIImage]) {
        imgView.image = images[0]
        img1.image = images[1]
        img2.image = images[2]
        img3.image = images[3]
    }


    @IBAction func didClickSelectImage(_ sender: Any) {
        
//        imagePicker.allowsEditing = false
//        imagePicker.sourceType = .photoLibrary
//
//        present(imagePicker, animated: true, completion: nil)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }

    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgView.contentMode = .scaleAspectFit
            imgView.image = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
let a = ViewController()

