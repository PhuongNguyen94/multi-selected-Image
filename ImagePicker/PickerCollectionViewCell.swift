//
//  PickerCollectionViewCell.swift
//  ImagePicker
//
//  Created by Nhân Phùng on 1/24/18.
//  Copyright © 2018 YangCS. All rights reserved.
//

import UIKit
import Photos

class PickerCollectionViewCell: UICollectionViewCell {
    

    var imageView: UIImageView!
    
  public func setItem(asset: PHAsset) {
    let imgManager = PHImageManager.default()
    let requestOptions = PHImageRequestOptions()
    requestOptions.isSynchronous = true
        
        imgManager.requestImage(for: asset, targetSize: UIScreen.main.bounds.size, contentMode: .aspectFill, options: requestOptions, resultHandler: { (uiimage, info) in
            
            self.imageView = UIImageView.init(frame:self.bounds)
            self.imageView.image = uiimage
            self.addSubview(self.imageView)
            
        })
    }
}
